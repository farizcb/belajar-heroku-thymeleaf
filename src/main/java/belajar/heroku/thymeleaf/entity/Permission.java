package belajar.heroku.thymeleaf.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 26/11/2020
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "m_permission")
@Data
public class Permission extends BaseEntity {

    @Column(nullable = false)
    private String label;

    @Column(nullable = false, unique = true)
    private String value;

    @CreatedDate
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime modifiedDate;

}
