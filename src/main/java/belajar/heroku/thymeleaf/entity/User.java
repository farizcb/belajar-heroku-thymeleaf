package belajar.heroku.thymeleaf.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 26/11/2020
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "m_user")
@Data
public class User extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private Boolean active = Boolean.TRUE;

}
