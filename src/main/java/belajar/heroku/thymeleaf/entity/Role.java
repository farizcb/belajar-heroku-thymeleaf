package belajar.heroku.thymeleaf.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 26/11/2020
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "m_role")
@Data
public class Role extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String name;

    private String description;

    @ManyToMany
    @OrderBy("value asc")
    @JoinTable(
            name="c_security_role_permission",
            joinColumns=@JoinColumn(name="id_role", nullable=false),
            inverseJoinColumns=@JoinColumn(name="id_permission", nullable=false)
    )
    private Set<Permission> permissionSet = new HashSet<>();

    @CreatedDate
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime modifiedDate;

}
