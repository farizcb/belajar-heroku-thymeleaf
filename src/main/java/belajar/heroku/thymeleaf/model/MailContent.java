package belajar.heroku.thymeleaf.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 13/03/2021
 */

@Getter
@Setter
public class MailContent {

    private String sentTo;
    private String subject;
    private String content;

}
