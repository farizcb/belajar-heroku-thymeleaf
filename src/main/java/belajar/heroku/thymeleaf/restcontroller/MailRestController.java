package belajar.heroku.thymeleaf.restcontroller;

import belajar.heroku.thymeleaf.model.MailContent;
import belajar.heroku.thymeleaf.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 13/03/2021
 */

@RestController
@RequestMapping(value = "api/mail", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class MailRestController {

    private final MailService mailService;

    @PostMapping("send")
    public void sendEmail(@RequestBody MailContent mailContent) {
        mailService.sendEmail(mailContent);
    }

}
