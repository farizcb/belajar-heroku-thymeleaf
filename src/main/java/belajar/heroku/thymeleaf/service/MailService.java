package belajar.heroku.thymeleaf.service;

import belajar.heroku.thymeleaf.model.MailContent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 13/03/2021
 */

@Service
@Slf4j
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender javaMailSender;

    public void sendEmail(MailContent mailContent) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            log.info("Mengirim email ke : [{}] dengan subject : [{}]", mailContent.getSentTo(), mailContent.getSubject());

            helper.setTo(mailContent.getSentTo());
            helper.setSubject(mailContent.getSubject());
            helper.setText(mailContent.getContent(), false);
            javaMailSender.send(message);
            log.info("=== berhasil kirim email ke : [" + mailContent.getSentTo() + "] ===");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
